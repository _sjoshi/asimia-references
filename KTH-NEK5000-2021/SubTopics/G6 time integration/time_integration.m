% bdf scheme for Nek5000
%
% BDF/EXT: 
% 11/6 y^n+1 = 3 y^n - 3/2 y^n-1 + 1/3 y^n-2 + dt f^n+1
% f^n+1 = g^n+1 + h^n+1 = g^n+1 + (3h^n -3h^n-1 + 1h^n-2)
%
% AB
% y^n+1 = y^n + dt*(23/12*f^n -4/3*f^n-1 + 5/12 f^n-2)
%
% for an equation of the form y' = f(y) = g(y) + h(y)
% with f=g+h and g treated implicit and h explicit.
%
% BDF1:
% y^n+1 = y^n + dt f^n+1
% EXT1:
% f^n+1 = g^n+1 + h^n+1 = g^n+1 + h^n   ! IS THIS f^n or h^n???
% BDF1/EXT1:
% y^n+1 = y^n + dt*(g^n+1 + f^n)
% y^n+1 - dt*g^n+1 = y^n + dt*h^n
%
% BDF2:
% 3/2 y^n+1 =  2 y^n - 1/2 y^n-1 + dt f^n+1
% EXT2:
% f^n+1 = g^n+1 + h^n+1 = g^n+1 + (2h^n - h^n-1)
% larger stability: 8/3  -7/3  2/3
% 3/2 y^n+1 =  2 y^n - 1/2 y^n-1 + dt (g^n+1 + 2h^n - h^n-1)
% 3/2 y^n+1 - dt*g^n+1 =  2 y^n - 1/2 y^n-1 + dt (2h^n - h^n-1)
% AB2:
% y^n+1 = y^n + dt*(3/2 f^n - 1/2 f^n-1)
%
% BDF3:
% 11/6 y^n+1 = 3 y^n - 3/2 y^n-1 + 1/3 y^n-2 + dt f^n+1
% EXT3:
% f^n+1 = g^n+1 + h^n+1 = g^n+1 + (3h^n -3h^n-1 + 1h^n-2)
% 11/6 y^n+1 =  3y^n - 3/2y^n-1 + 1/3y^n-2 + dt (g^n+1 + 3h^n - 3h^n-1 + 1h^n-2)
% 11/6 y^n+1 -dt*g^n+1 =  3y^n - 3/2y^n-1 + 1/3y^n-2 + dt (3h^n - 3h^n-1 + 1h^n-2)

clear all
close all

% Coefficients for BDF scheme
bd(:,1) = [  1   1   0   0  ]; % BDF1
bd(:,2) = [ 3/2  2 -1/2  0  ]; % BDF2
bd(:,3) = [ 11/6 3 -3/2 1/3 ]; % BDF3

ab(:,1) = [ 1  0 0 ]; % EXT1
ab(:,2) = [ 2 -1 0 ]; % EXT2
ab(:,3) = [ 3 -3 1 ]; % EXT3
% uncomment the following for BDF3/EXT2a
ab(:,3) = [8/3 -7/3 2/3]; % EXT2a

% test integration of simple equation
% y' = f(y) = g(y) + h(y)
% g(y) = exp(alpha y)   h(y) = exp(beta y)
%
% exact solution: y' = (alpha+beta) y
% y(t) = y0*exp( (alpha+beta) * t)


for j=1:6
    
    fact = 2^(j-1);
    
    y(1) = 1;
    beta = 1;      % EXT
    alpha = 1;     % BDF
    dt = 0.01/fact;
    dtt(j) = dt;
    t(1)=0;
    for i=1:100*fact
        norder = min(i,3);
        norder1 = min(norder,3);   % for EXT
        norder2 = min(norder,3);   % for BDF
        
        h(i) = beta * y(i);
        %h(i) = sin(y(i)^2);

        % extrapolation scheme
        if norder1==1
            f(i) = ab(1,norder1)*h(i);
        elseif norder1==2
            f(i) = ab(1,norder1)*h(i) + ab(2,norder1)*h(i-1);
        elseif norder1==3
            f(i) = ab(1,norder1)*h(i) + ab(2,norder1)*h(i-1) + ab(3,norder1)*h(i-2);
        end
        
        % backward differencing scheme
        if norder2==1
            ff(i) = bd(2,norder2)*y(i) + dt*f(i);
        elseif norder2==2
            ff(i) = bd(2,norder2)*y(i) + bd(3,norder2)*y(i-1) + ...
                dt*f(i);
        elseif norder2==3
            ff(i) = bd(2,norder2)*y(i) + bd(3,norder2)*y(i-1) + ...
                bd(4,norder2)*y(i-2) + dt*f(i);
        end
        y(i+1) = 1/(bd(1,norder2)-dt*alpha)*ff(i);
        
        t(i+1) = t(i) + dt;  
               
        if (i<2)   
           % without overwriting the first step, only second order possible
           % i.e. the scheme is only as accurate as the boundary scheme + 1
           y(i+1)=y(1) * exp( (alpha+beta)*t(i+1));
        end
 
        
    end
    
    yex = y(1) * exp( (alpha+beta)*t);
    
    figure(1);hold on
    plot(t,y)
    plot(t,yex,'r--')
    
    yend(j) = y(end);
    
 
    
    
end

err1 = abs(yend-yex(end));
err2 = abs(yend-yend(end));


% compute order of the scheme
% err= C*dt^p
%
% log(err1/err2) = p*log(dt1/dt2)

disp('Order computed based on exact solution:')
log(err1(1:end-1)./err1(2:end))./log(dtt(1:end-1)./dtt(2:end))
disp('Order computed based on finest solution:')
log(err2(1:end-1)./err2(2:end))./log(dtt(1:end-1)./dtt(2:end))



% TORDER=1
% 
%  istep,nab,nbd = 1,1,1
%  ab = 1.      0.      0.  
%  bd = 1.      1.      0.      0.
% 
%  istep,nab,nbd = 2,2,1
%  ab = 3/2     -1/2    0.  
%  bd = 1.      1.      0.      0.        
% 
%  istep,nab,nbd = 3,3,1
%  ab = 23/12   -4/3    5/12  
%  bd = 1.      1.      0.      0.        
% 
% TORDER=2
% 
%  istep,nab,nbd = 1,1,1
%  ab = 1.      0.      0. 
%  bd = 1.      1.      0.      0.       
% 
%  istep,nab,nbd = 2,2,2
%  ab = 2.      -1.     0. 
%  bd = 3/2     2.      -1/2    0.
% 
%  istep,nab,nbd = 3,3,2
%  ab = 8/3     -7/3    2/3 
%  bd = 3/2     2.      -1/2    0.
% 
% 
% TORDER=3
% 
%  istep,nab,nbd = 1,1,1
%  ab = 1.      0.      0. 
%  bd = 1.      1.      0.      0.        
% 
%  istep,nab,nbd = 2,2,2
%  ab = 2.      -1.     0.
%  bd = 3/2     2.      -1/2    0.       
% 
%  istep,nab,nbd = 3,3,3
%  ab = 3.      -3.     1. 
%  bd = 11/6    3.      -3/2    1/3        

   



