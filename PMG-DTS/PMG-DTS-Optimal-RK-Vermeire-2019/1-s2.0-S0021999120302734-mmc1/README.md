Data for Optimal Embedded Pair Runge-Kutta Schemes for Pseudo Time-Stepping. B. C. Vermeire, N. A. Loppi, P. E. Vincent. Journal of Computational Physics

tableaus/ Butcher tableaus of the form k_s_shat_*.txt where * is either A, b, or bhat and k is the polynomial degree for FRDG
patch/ Patch file for PyFR
cases/ Configuration and mesh files for PyFR cylinder example case